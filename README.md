# Weather Interface
View weather data collected through beaglebone.

## Setup Guide
1. `git clone` this repository to your host machine (beaglebone or otherwise).
2. `sudo apt-get install build-essential nodejs npm` - install dependencies (on Debian)
    - NodeJS and NPM
    - Build Essentials
3. `cd weather_interface`.
4. `nano .env` or `vi .env` and adjust the parameters to fit your needs. Make sure that the CSV pointers are aimed at wherever your `weather_collect` is exporting them to.
5. `npm install` - installs nodejs dependencies.
6. `npm install -g forever` - install forever to keep the server running.
7. `forever start index.js` - start the server with forever.
8. Navigate to your machines IP or domain and see if the web front-end is visible on the port you defined in `.env`.

## Additional Setup
These things arent required for functionality, but will make the beaglebone much more stable in the long term.
1. Disable On-board storage. By default, the beaglebone will try to boot from its built-in storage instead of an SD card. To make sure that the beaglebone boots from SD when restarted, follow these steps:
    1. `fdisk /dev/mmcblk1` - double check that your on-board storage is called mmcblk1
    2. Use the `a` command to toggle the bootable flag.
    3. Use the `w` command to apply.
    4. Run `sync` and then `reboot now`. Your beaglebone should reboot to SD automatically, with its on-board boot disabled.

2. Set up the weather-interface as a systemd service. This will put systemd in charge of keeping the webserver up; it will start automatically on boot, and restart automatically when it crashes (in most cases). Create the file `/etc/systemd/system/weather_interface.service` and edit it to have the following contents:
```
[Unit]
Description=Weather Interface Node Server
Documentation=https://code.cs.earlham.edu/hip/weather_interface
After=network.target

[Service]
Type=simple
User=debian
WorkingDirectory=/home/debian/weather_interface
ExecStart=/usr/bin/node index.js
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

## Usage
Uses Nodejs and npm, which should come pre-installed on the beaglebone version of debian linux. 
Install dependencies with `npm install`, and run webserver with `node index` or `forever start index` to run as a detatched session with crash restarts.

## .env File

__Note__: This .env should not contain any credentials or sensetive information, and does not need to be gitignored.

Set up `.env` file following the template below:
```
HOSTNAME=Beaglebone name                    // Identifies host machine on front-end
CSV_CURRENT=data-dir/current-weather.csv        // Path to current weather csv (default)
CSV_HISTORY=data-dir/history-weather.csv        // Path to all weather csv (default)
PORT=9900                                   // Port to be used for webserver
```
