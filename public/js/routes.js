// ----------- ROUTE FUNCTIONS ----------- //

async function get_history_weather(depth = 100, offset = 0){ // get weather history CSV from server and load it into the DATA array.
    $.ajax({
        type: 'GET',
        url: '/history-weather',
        success: function(response) { 
            data = response;
            if (unit_system == 0){
                make_graph(data, depth);
            }
            else{
                let toKM = [
                    data[0].indexOf("windSpeed"),
                    data[0].indexOf("windSpeedAvg"),
                    data[0].indexOf("windSpeed2mAvg")
                ];
                let toC = [
                    data[0].indexOf("insideTemp"),
                    data[0].indexOf("outsideTemp")
                ];
                for (col in toC){
                    let col_ind = toC[col];
                    for (x = 1; x < data.length; x++){
                        data[x][col_ind] = Math.round((((data[x][col_ind] - 32) * (5/9)) + Number.EPSILON) * 100) / 100;
                    }
                }
                for (col in toKM){
                    let col_ind = toKM[col];
                    for (x = 1; x < data.length; x++){
                        data[x][col_ind] = Math.round(((data[x][col_ind] * 1.60934) + Number.EPSILON) * 100) / 100;
                    }
                }
                make_graph(data, depth);
            }
            
        },
        error: function(xhr, status, err) {
            console.log(xhr.responseText);
        }
    });
}
async function get_host(){
    $.ajax({
        type: 'GET',
        url: '/hostname',
        success: function(response) { 
            if (document.getElementById("hostname")){ // display hostname
                document.getElementById("hostname").innerHTML = "<u>" + response + "</u> Weather Portal";
            }
            document.title = "Weather Frontend (" + response + ")";
        },
        error: function(xhr, status, err) {
            console.log(xhr.responseText);
        }
    });
}
function get_vers(){
    $.ajax({
        type: 'GET',
        url: '/version',
        success: function(response) { 
            if (document.getElementById("vnum")){ // display hostname
                document.getElementById("vnum").innerHTML = "Version " + response;
            }
        },
        error: function(xhr, status, err) {
            console.log(xhr.responseText);
        }
    });
}
function get_current_weather(){
    $.ajax({
        type: 'GET',
        url: '/current-weather',
        success: function(response) { 
            data = response;
            if (unit_system == 0){
                make_display(data);
            }
            else{
                let toKM = [
                    data[0].indexOf("windSpeed"),
                    data[0].indexOf("windSpeedAvg"),
                    data[0].indexOf("windSpeed2mAvg")
                ];
                let toC = [
                    data[0].indexOf("insideTemp"),
                    data[0].indexOf("outsideTemp")
                ];
                for (col in toC){
                    let col_ind = toC[col];
                    for (x = 1; x < data.length; x++){
                        data[x][col_ind] = Math.round((((data[x][col_ind] - 32) * (5/9)) + Number.EPSILON) * 100) / 100;
                    }
                }
                for (col in toKM){
                    let col_ind = toKM[col];
                    for (x = 1; x < data.length; x++){
                        data[x][col_ind] = Math.round(((data[x][col_ind] * 1.60934) + Number.EPSILON) * 100) / 100;
                    }
                }
                make_display(data);
            }
        },
        error: function(xhr, status, err) {
            //console.log(xhr.responseText);
            alert("Current Weather Failed to load. Wait for server to collect weather information, or check config.")
        }
    });
}
function create_csv(){
    $.ajax({
        type: 'GET',
        url: '/history-weather',
        success: function(response) { 
            console.info("Creating CSV for current weather...");
            data = response;
            if (unit_system != 0){
                let toKM = [
                    data[0].indexOf("windSpeed"),
                    data[0].indexOf("windSpeedAvg"),
                    data[0].indexOf("windSpeed2mAvg")
                ];
                let toC = [
                    data[0].indexOf("insideTemp"),
                    data[0].indexOf("outsideTemp")
                ];
                for (col in toC){
                    let col_ind = toC[col];
                    for (x = 1; x < data.length; x++){
                        data[x][col_ind] = Math.round((((data[x][col_ind] - 32) * (5/9)) + Number.EPSILON) * 100) / 100;
                    }
                }
                for (col in toKM){
                    let col_ind = toC[col];
                    for (x = 1; x < data.length; x++){
                        data[x][col_ind] = Math.round(((data[x][col_ind] * 1.60934) + Number.EPSILON) * 100) / 100;
                    }
                }
            }
            // Build CSV content
            let csvContent = "data:text/csv;charset=utf-8," 
                + data.map(e => e.join(",")).join("\n");
            var encodedUri = encodeURI(csvContent);
            var link = document.createElement("a");
            link.setAttribute("href", encodedUri);
            link.setAttribute("download", "current_weather.csv");
            document.body.appendChild(link); // Required for FF
            
            link.click(); // This will download the data file named "my_data.csv".
        },
        error: function(xhr, status, err) {
            console.log(xhr.responseText);
        }
    });
}