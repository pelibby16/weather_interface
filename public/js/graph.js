// Colors and Data


// ----------- RENDER GRAPH ----------- //
function make_graph(arr, depth, offset){
    data = arr;
    if (chart){
        chart.destroy(); // clear old information so it doesnt overflow
    }

    let stamp_index = arr[0].indexOf("timestamp");
    console.log(stamp_index);
    console.log(arr[1][stamp_index]);
    console.log(arr[arr.length - 1][stamp_index]);
    
    var oldest = arr[1][stamp_index];
    var newest = arr[arr.length - 1][stamp_index];

    // update sliders
    /*
    document.getElementById('slide1').setAttribute("max", arr.length);
    document.getElementById('slide2').setAttribute("max", arr.length);
    slide_max = Math.max(document.getElementById('slide1').value, document.getElementById('slide2').value);
    slide_min = Math.min(document.getElementById('slide1').value, document.getElementById('slide2').value);
    arr = [arr[0]].concat(arr.slice(slide_min, slide_max));
    */
    arr = [arr[0]].concat(arr.slice(1).slice(arr.length - depth));
    
    let data_sensors = arr.shift(); // titles of columns
    let datasets = []; // container for data to graph (2d array)
    l = [] // container for dates (displayed on x axis)

    for (x=0;x< arr.length;x++){
        l.push(arr[x][1])
    }
    console.log(data_sensors);
    for (x=2;x<data_sensors.length;x++){ // start from two for timestamp
        // Add units
        if (data_sensors[x] == "barometric") { data_sensors[x] += " (inHg)"}
        if (data_sensors[x] == "insideTemp") { data_sensors[x] += " (°" + temp_unit[unit_system] + ")"}
        if (data_sensors[x] == "outsideTemp") { data_sensors[x] += " (°" + temp_unit[unit_system] + ")"}
        if (data_sensors[x] == "insideHum") { data_sensors[x] += " (%)"}
        if (data_sensors[x] == "outsideHum") { data_sensors[x] += " (%)"}
        if (data_sensors[x] == "windSpeed") { data_sensors[x] += " (" + speed_unit[unit_system] + ")"}
        if (data_sensors[x] == "windSpeedAvg") { data_sensors[x] += " (" + speed_unit[unit_system] + ")"}
        if (data_sensors[x] == "windSpeed2mAvg") { data_sensors[x] += " (" + speed_unit[unit_system] + ")"}
        if (data_sensors[x] == "solarRad\r") { data_sensors[x] += " (W/m^2)"}

        d = []
        for (y=0;y< arr.length;y++){
            d.push(arr[y][x])
        }
        datasets.push({ // create dataset (one data set is temp, one is humidity, etc)
            data: d,
            label: data_sensors[x],
            borderColor: colors[x-2],
            fill: false
        })
    }
    

    chart = new Chart(document.getElementById("history-weather-chart"), { // build chart with chartjs
        type: 'line',
        options: {
            plugins: {
                title: {
                    display: true,
                    text: 'Weather from ' + oldest + " to " + newest
                }
            }
        },
        data: {
            labels: l,
            datasets: datasets
        }
    });
}

// RANGE SLIDER 
/*
function getVals(){
    // Get slider values
    var parent = this.parentNode;
      var slide1 = document.getElementById('slide1').value;
      var slide2 = document.getElementById('slide2').value;
    // Neither slider will clip the other, so make sure we determine which is larger
    if( slide1 > slide2 ){ var tmp = slide2; slide2 = slide1; slide1 = tmp; }
    
    var displayElement = parent.getElementsByClassName("rangeValues")[0];
    console.log(slide1, slide2, data.length);
    displayElement.innerHTML = data[50][1] + " - " + data[50][1];
    get_history_weather(); 
}
*/

// ----------- RUN ON DOCUMENT LOAD ----------- //
$( document ).ready(function() {
    get_history_weather(); 
    get_host();
    document.getElementById("loading").style.display = "none"; // hide placeholder on load
    console.log( "ready!" );

    document.getElementById("home-button").addEventListener("click", function(event){
        window.location.href = window.location.href.substring(0, window.location.href.lastIndexOf('/')); // switch back to home page
    });

    document.getElementById("units-button2").addEventListener("click", function(event){
        if (unit_system == 0){
            unit_system = 1;
            document.getElementById("units-button2").innerText = "Switch to Imperial";
        }
        else{
            unit_system = 0;
            document.getElementById("units-button2").innerText = "Switch to Metric";
        }
        get_history_weather()
    });
    

});
