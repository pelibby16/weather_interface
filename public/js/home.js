// ----------- RENDER display ----------- //
function make_display(arr){
    data = {};
    for (x = 0; x < arr[0].length;x++){
        title = arr[0][x];
        value = arr[1][x];
        data[title] = value;
    }

    // timestamp
    document.getElementById("lastrecord").innerText = "Recorded at: " + data['timestamp'];

    // Build out various displays
    if (data["status"] == 'no data'){ // if status=no data, show error message.
        document.getElementById("error-bar").innerText = "Host was not able to collect data from Davis Vantage Pro. Check your system."
    }
    else{ // status is normal
        // create each section of current weather report
        temptable(data);
        forecast(data);
        windtable(data);
        lighttable(data);
    }
}
function forecast(data){ // forcast section of current weather
    forcast_elem = document.getElementById("current-weather-forecast");
    forcast_elem.innerHTML = ""; //reset
    forcast_elem.innerHTML += "<span class='key'>Instant Forecast</span><br>";
    forcast_elem.innerHTML += "<span class='value'>" + data['forecast'] + "</span>";
}
function temptable(data){ // temperature section of current weather
    temptable_elem = document.getElementById("current-weather-table-temp");
    temptable_elem.innerHTML = ""; // reset table
    const header = document.createElement("tr");
    const thead = document.createElement("thead");
    const tbody = document.createElement("tbody");
    const row = document.createElement("tr");

    // insideTemp
    header.innerHTML += "<th>Inside Temp</th>";
    row.innerHTML += "<td>" + data['insideTemp'] + " °" + temp_unit[unit_system] + "</td>";
    

    // insideHum
    header.innerHTML += "<th>Inside Humidity</th>";
    row.innerHTML += "<td>" + data['insideHum'] + "%</td>";

    // outsideTemp
    header.innerHTML += "<th>Outside Temp</th>";
    row.innerHTML += "<td>" + data['outsideTemp'] + " °" + temp_unit[unit_system] + "</td>";

    // outsideHum
    header.innerHTML += "<th>Outside Humidity</th>";
    row.innerHTML += "<td>" + data['outsideHum'] + "%</td>";

    // barometric
    header.innerHTML += "<th>Barometric Pressure</th>";
    row.innerHTML += "<td>" + data['barometric'] + " (" + data['baroTrend'] + ")</td>";

    // append to parent element
    thead.appendChild(header);
    temptable_elem.appendChild(thead);
    tbody.appendChild(row);
    temptable_elem.appendChild(tbody); 
}
function windtable(data){ // wind section of current weather
    windtable_elem = document.getElementById("current-weather-table-wind");
    windtable_elem.innerHTML = ""; // reset table
    const header = document.createElement("tr");
    const thead = document.createElement("thead");
    const tbody = document.createElement("tbody");
    const row = document.createElement("tr");

    // wind speed
    header.innerHTML += "<th>Wind Speed</th>";
    row.innerHTML += "<td>" + data['windSpeed'] + " " + speed_unit[unit_system] + "</td>";

    // wind speed avg
    header.innerHTML += "<th>Wind Speed Average</th>";
    row.innerHTML += "<td>" + data['windSpeedAvg'] + " " + speed_unit[unit_system] + "</td>"; 

    // wind speed 2 minute average
    header.innerHTML += "<th>Wind Speed (2 min avg)</th>";
    row.innerHTML += "<td>" + data['windSpeed2mAvg'] + " " + speed_unit[unit_system] + "</td>"; 

    // wind chill
    header.innerHTML += "<th>Wind Direction</th>";
    row.innerHTML += "<td>" + data['windDir'] + "</td>";

    // append to parent element
    thead.appendChild(header);
    windtable_elem.appendChild(thead);
    tbody.appendChild(row);
    windtable_elem.appendChild(tbody); 
}
function lighttable(data){ // light section of current weather
    lighttable_elem = document.getElementById("current-weather-table-light");
    lighttable_elem.innerHTML = ""; // reset table
    const header = document.createElement("tr");
    const thead = document.createElement("thead");
    const tbody = document.createElement("tbody");
    const row = document.createElement("tr");
    // Sunrise
    header.innerHTML += "<th>Sunrise</th>";
    row.innerHTML += "<td>" + data['sunrise'] + " AM</td>";

    // Sunset
    header.innerHTML += "<th>Sunset</th>";
    row.innerHTML += "<td>" + data['sunset'] + " PM</td>";

    // UV Level
    header.innerHTML += "<th>UV Level</th>";
    row.innerHTML += "<td>" + data['UVLevel'] + "</td>";

    // Solar Radiation
    header.innerHTML += "<th>Solar Radiation</th>";
    row.innerHTML += "<td>" + data['solarRad'] + " W/m^2</td>";

    // append to parent element
    thead.appendChild(header);
    lighttable_elem.appendChild(thead);
    tbody.appendChild(row);
    lighttable_elem.appendChild(tbody); 
}

// ----------- RUN ON START  ----------- //
$( document ).ready(function() {
    get_current_weather(); // get initial state for load
    get_vers(); // get webapp version from node server

    setInterval(() => {
        console.info("refreshing current weather...")
        get_current_weather();
    }, 1000*60*5); // refresh weather every 5 minutes
    
    get_host();
    console.log( "ready!" );
});