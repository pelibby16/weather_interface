// ----------- Button Events ----------- //

$( document ).ready(function() {
    document.getElementById("csv-button").addEventListener("click", function(event){
        create_csv(); // download CSV weather history through Web
    });
    document.getElementById("graph-button").addEventListener("click", function(event){
        window.location.href = window.location.href + "graph"; // switch to graph page
    });
    document.getElementById("units-button").addEventListener("click", function(event){
        if (unit_system == 0){
            unit_system = 1;
            document.getElementById("units-button").innerText = "Switch to Imperial";
        }
        else{
            unit_system = 0;
            document.getElementById("units-button").innerText = "Switch to Metric";
        }
        get_current_weather()
    });
    
});

