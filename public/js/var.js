var unit_system = 0;
var speed_unit = ["Mph", "KM/h"];
var temp_unit = ["F", "C"];
colors = [
    "#FF5A33", // barometric pressure
    "#FFEC5C", // inside temp
    "#B4CF66", // inside humidity
    "#44803F", // outside temp
    "#146152", // outside humidity
    "#0033EE", // windspeed
    "#0088DD", // windspeed avg
    "#00dd00", // windspeed 2min avg
    "#dd00dd", // UV level
    "#6600ee", // solar radiation
]
data = [];
chart = null;