var pjson = require('./package.json');
console.log("======================================================================================");
console.log("|                                                                                    |");
console.log("| ███████  ██████         ██     ██ ███████  █████  ████████ ██   ██ ███████ ██████  |");
console.log("| ██      ██              ██     ██ ██      ██   ██    ██    ██   ██ ██      ██   ██ |");
console.log("| █████   ██              ██  █  ██ █████   ███████    ██    ███████ █████   ██████  |");
console.log("| ██      ██              ██ ███ ██ ██      ██   ██    ██    ██   ██ ██      ██   ██ |");
console.log("| ███████  ██████          ███ ███  ███████ ██   ██    ██    ██   ██ ███████ ██   ██ |");
console.log("|                                                                                    |");
console.log("========================================================================== v" + pjson.version + " ==");
console.log("\n");

// Require important Node things
const express = require('express');
const app = express();
require('dotenv').config(); // used for configuration.

// set up EJS (Though I don't think I've used it yet).
app.set('view engine', 'ejs');
app.get('/', function(req, res) { res.render("home",{})});
app.get('/graph', function(req, res) { res.render('graph.ejs'); });


app.use(express.static(__dirname + '/public')); //use public folder for all client side JS and CSS.

// CSV Processing
app.get('/current-weather', function (req, res) { // get current weather file, as dictated in .env file
    var data = require("fs").readFileSync(process.env.CSV_CURRENT, "utf8")
    data = data.split("\n")
    arr = []
    for(x = 0; x < data.length;x++){ arr.push(data[x].split(','))}
    res.send(arr)
})

app.get('/history-weather', function (req, res) { // get history weather file, as dictated in .env file
    var data = require("fs").readFileSync(process.env.CSV_HISTORY, "utf8")
    data = data.split("\n")
    arr = []
    for(x = 0; x < data.length;x++){ arr.push(data[x].split(','))}
    res.send(arr)
})

// host meta inf
app.get('/hostname', function (req, res) { // get host information so that the client side makes more sense
    if (process.env.HOSTNAME){
        res.send(process.env.HOSTNAME)
    }else{
        var os = require("os");
        var hostname = os.hostname();
        res.send(hostname)
    }
})
app.get('/version', function (req, res) { // get host information so that the client side makes more sense
    res.send(pjson.version);
})


// LISTEN
app.listen(process.env.PORT || 9900, function(){ // start on .env defined port, or fall back to 9900
    console.log("SERVER: Listening on port %d", this.address().port);
});
